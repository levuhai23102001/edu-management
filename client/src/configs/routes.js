const routes = {
  home: "/",
  login: "/login",
  register: "/register",
  dashboard: "/dashboard",
};

export default routes;
