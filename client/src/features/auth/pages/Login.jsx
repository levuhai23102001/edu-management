import React from "react";

const Login = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        width: "100%",
        height: "100vh",
      }}
    >
      <h1>Login Form ✨</h1>
      <form style={{ display: "flex", flexDirection: "column" }}>
        <label>Email</label>
        <input type="email" name="email" placeholder="Email" required />
        <label>Password</label>
        <input type="password" name="password" required />
        <button>Login</button>
      </form>
    </div>
  );
};

export default Login;
