import React from "react";
import PropTypes from "prop-types";
import Sidebar from "../components/Sidebar";

const MainLayout = ({ children }) => {
  return (
    <div className="wrapper">
      <Sidebar />
      <div className="main-container">{children}</div>
    </div>
  );
};

export default MainLayout;

MainLayout.propTypes = {
  children: PropTypes.node.isRequired,
};
