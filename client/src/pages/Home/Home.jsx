import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <>
      <h1>Home Page</h1>
      <Link style={{ marginRight: 20 }} to="/login">
        Login
      </Link>
      <Link to="/register">Register</Link>
    </>
  );
};

export default Home;
