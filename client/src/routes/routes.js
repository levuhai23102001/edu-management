import configs from "../configs";

import Login from "../features/auth/pages/Login";
import Register from "../features/auth/pages/Register";
import Dashboard from "../pages/Dashboard/";

const authRoutes = [
  { path: configs.routes.login, component: Login },
  { path: configs.routes.register, component: Register },
];

const privateRoutes = [
  { path: configs.routes.dashboard, component: Dashboard },
];

export { authRoutes, privateRoutes };
